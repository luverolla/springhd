const client = Stomp.over(new SockJS("/reqdt"));
const opts = {
	hour12: false,
	month: "numeric",
	day: "numeric",
	year: "numeric",
	timeZoneName: "short",
	hour: "2-digit",
	minute: "2-digit",
	second: "2-digit"
};

client.debug = null;
client.connect({}, function(f)
{
    client.subscribe('/topic/datetime', function(messageOutput)
	{
		let dateStr = messageOutput.body;
        document.querySelector("#datetime").innerHTML = "Server time: " + dateStr;

        if(document.querySelector("#datetime_2"))
            document.querySelector("#datetime_2").innerHTML = dateStr;
    });
});