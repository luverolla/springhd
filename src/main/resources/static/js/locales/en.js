if(!window.SHD_LOCALES)
    window.SHD_LOCALES = {};
    
window.SHD_LOCALES["en"] =
{
    PLACEHOLDER_SINGLE: "select an option",
	PLACEHOLDER_MULTI: "select options",
	SEARCH: "search",
	NO_RESULT: "no result",

    OPEN_PICKER: "open picker",
    CHOOSE_DATE: "choose date",
    PREV_YEAR: "previous year",
    PREV_MONTH: "previous month",
    NEXT_MONTH: "next month",
    NEXT_YEAR: "next year",
    
    CHOOSE_FILES: "Choose file(s)",
    CLEAR_FILES: "Clear file(s)",
    N_FILES: "{1} file(s)",

    GOTO_FIRST: "go to first page",
    GOTO_PREV: "go to previous page",
    GOTO_PAGE: "go to page {NUM}",
    GOTO_NEXT: "go to next page",
    GOTO_LAST: "go to last page",
    SHOWING_TO: "showing {FROM} to {TO} of {SIZE}",
    NOT_ACTIVE: "not active",
    ASC_ACTIVE: "ascending order active",
    DESC_ACTIVE: "descending order active",
    PER_PAGE: "rows per page",

    PREVIEW: "preview",
    
    DAYS: ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"],

    ERR_GEN: "This field has errors",
    ERR_SPEC:
    {
        "required": "this field is mandatory",
        "minval": "select at least {minval} options",
        "maxval": "select up to {maxval} options",
        "eqval": "select {eqval} options",
        "pattern": "use the pattern: {example}",
        "min": "insert a value bigger, or equal to {min}",
        "max": "insert a value smaller or equal to {max}",
        "minlen": "insert a value long, at least, {minlen} characters",
        "maxlen": "insert a value up to {maxlen} characters long",
        "eqlen": "insert a value long exactly {eqlen} characters",
        "minsize": "insert file(s), big at least {minsize}",
        "maxsize": "insert file(s), up to {maxsize} big"
    }
}