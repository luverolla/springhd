if(!window.SHD_LOCALES)
    window.SHD_LOCALES = {};

window.SHD_LOCALES["it"] =
{
    OPEN_PICKER: "aprire il calendario",
    CHOOSE_DATE: "scegliere una data",
    PREV_YEAR: "anno precedente",
    PREV_MONTH: "mese precedente",
    NEXT_MONTH: "mese successivo",
    NEXT_YEAR: "anno successivo",
    DAYS: ["Lunedì", "Martedì", "Mercoledì", "Giovedì", "Venerdì", "Sabato", "Domenica"],

    PLACEHOLDER_SINGLE: "scegliere un'opzione",
	PLACEHOLDER_MULTI: "scegliere una o più opzioni",
	SEARCH: "cercare",
	NO_RESULT: "nessun risultato",
    
    CHOOSE_FILES: "Scegli file",
    CLEAR_FILES: "Cancella file",
    N_FILES: "{1} file",

    GOTO_FIRST: "a pagina 1",
    GOTO_PREV: "a pagina precedente",
    GOTO_PAGE: "a pagina {NUM}",
    GOTO_NEXT: "a pagina successiva",
    GOTO_LAST: "all'ultima pagina",
    SHOWING_TO: "da {FROM} a {TO} di {SIZE}",
    NOT_ACTIVE: "non attivo",
    ASC_ACTIVE: "ordine crescente attivo",
    DESC_ACTIVE: "ordine decrescente attivo",
    PER_PAGE: "righe per pagina",

    PREVIEW: "anteprima",
    
    ERR_GEN: "Questo campo ha errori",
    ERR_SPEC:
    {
        "required": "questo campo è obbligatorio",
        "minval": "selezionare almeno {minval} opzioni",
        "maxval": "selezionare fino a {maxval} opzioni",
        "eqval": "selezionare {eqval} opzioni",
        "pattern": "rispettare il formato: {example}",
        "min": "inserire un valore almeno pari a {min}",
        "max": "inserire un valore non più grande di {max}",
        "minlen": "inserire un valore lungo almeno {minlen} caratteri",
        "maxlen": "inserire un valore non più lungo di {minlen} caratteri",
        "eqlen": "inserire un valore lungo {eqlen} caratteri",
        "minsize": "inserire un file grande almeno {minsize}",
        "maxsize": "inserire un file non più grande di {maxsize}",
    }
}