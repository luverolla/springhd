/**
 * @typedef {Object} Option - select option object
 * @property {number} key - option's key
 * @property {string} title - option's main text
 * @property {string} value - option's value
 * @property {string} img - option's image path
 * @property {string} desc - option's description
 * @property {string} group - option's group
 * @property {boolean} disabled - indicates if option is disabled
 * @property {boolean} selected - indicates if option is selected
 */

window.S_TRIG = "[data-shd-replace='dropdown']";

/**
 * @type {string}
 */
const S_PREFIX = "shdDropdown";

/**
 * @param {number} rem 
 * @returns {number}
 */
function rem2px(rem) 
{
	let style = window.getComputedStyle(document.body, null).getPropertyValue('font-size');
	let fontSize = parseFloat(style);

	return rem * fontSize;
}

/**
 * 
 */
class Select
{
	/**
	 * @type {HTMLSelectElement}
	 */
	old;

	/**
	 * @type {HTMLDivElement}
	 */
	el;

	/**
	 * @type {string}
	 */
	id;

	/**
	 * @type {string}
	 */
	name;

	/**
	 * @type {string}
	 */
	value;

	/**
	 * @type {Option[]}
	 */
	options;

	/**
	 * @type {Option[]}
	 */
	shown;

	/**
	 * @type {Option}
	 */
	curr;

	/**
	 * @type {boolean}
	 */
	search;

	/**
	 * @type {boolean}
	 */
	isOpen;

	/**
	 * @type {boolean}
	 */
	disabled;

	/**
	 * @type {boolean}
	 */
	multiple;

	/**
	 * @type {string}
	 */
	placeholder;

	/**
	 * @type {boolean}
	 */
	keyEvent;

	/**
	 * @param {string} name
	 */
	static reload(name)
	{
		let obj = window.SHD_SELECT_ISTANCES[name];
		obj.reset(obj.old);
		obj.init();
	}

	/**
	 * @param {HTMLSelectElement} old
	 */
	constructor(old)
	{
		this.reset(old);
	}

	/**
	 * @param {HTMLSelectElement} old
	 */
	reset(old)
	{
		this.old = old;
		this.isOpen = false;
		this.options = [];
		this.shown = [];
		this.multiple = old.multiple || false;
		this.disabled = old.disabled || false;
		this.search = old.dataset.shdSearch || false;
		this.placeholder = old.dataset.shdPlaceholder;
		this.value = old.dataset.shdValue || "";

		this.name = old.name || Date.now().toString();
		if(this.name.includes('_'))
			this.name = this.name.split('_')[1];

		this.id = `${S_PREFIX}-${this.name}`;

		if(!this.placeholder)
			this.placeholder = this.multiple ?
				window.SHD_LOCALES[window.LANG].PLACEHOLDER_MULTI :
				window.SHD_LOCALES[window.LANG].PLACEHOLDER_SINGLE;
	}

	/**
	 * @param {number} key 
	 */
	focusOption(key)
	{
		let display = this.el.querySelector(`.${S_PREFIX}__display`),
			search = this.el.querySelector(`.${S_PREFIX}__search`),
			list = this.el.querySelector(`.${S_PREFIX}__list`),
			opts = this.el.querySelectorAll(`.${S_PREFIX}--option`),
			which = this.el.querySelector(`#${this.id}--${key}`);

		this.curr = this.options.find(o => o.key === key);
		list.scrollTop = which.offsetTop - rem2px(5);
		
		opts.forEach(o =>
		{
			let opId = parseInt(o.id[o.id.length - 1]);
			opId === key ? o.classList.add("focus") : o.classList.remove("focus");
		});

		if(this.search)
			search.setAttribute("aria-activedescendant", which.id);
		display.setAttribute("aria-activedescendant", which.id);

		this.printOptions(this.shown);
	}

	open()
	{
		let display = this.el.querySelector(`.${S_PREFIX}__display`),
			search = this.el.querySelector(`.${S_PREFIX}__search`);

		this.isOpen = true;
		this.el.classList.add("open");
		display.setAttribute("aria-expanded", "true");

		if(this.search)
		{
			search.focus();
			display.tabIndex = -1;
		}

		this.focusOption(this.curr.key);
	}

	close()
	{
		let display = this.el.querySelector(`.${S_PREFIX}__display`);
		display.removeAttribute("aria-activedescendant");

		this.el.classList.remove("open");
		display.setAttribute("aria-expanded", "false");
		display.tabIndex = 0;
		this.isOpen = false;
	}

	focusEvents()
	{
		window.addEventListener("mousedown", e =>
		{
			if(!this.el.contains(e.target))
				this.close();
		});

		window.addEventListener("focusin", e =>
		{
			if(!this.el.contains(e.target))
				this.close();
		})
	}

	isSelected(key)
	{
		return this.options
			.find(o => o.key === key).selected;
	}

	selectOption(key)
	{
		if(!this.options.find(o => o.key).selected)
			this.toggleSelect(key);
	}

	toggleAndClose(key)
	{
		this.toggleSelect(key);

		if(!this.multiple)
			this.close();
	}

	keyboardEvents()
	{
		this.el.addEventListener("keydown", e =>
		{
			let last = this.shown.length - 1,
				curr = this.shown.indexOf(this.curr);

			switch(e.key)
			{
				case "Enter":
					e.preventDefault();
					this.isOpen ? this.toggleAndClose(this.curr.key) : this.open();
					break;

				case "Escape":
					if(this.isOpen)
						this.close();
					break;

				case "PageUp":
					if(this.isOpen)
					{
						e.preventDefault();
						this.keyEvent = true;
					
						this.curr = this.shown[0];
						this.focusOption(this.curr.key);

						if(!this.multiple && !this.isSelected(this.curr.key))
							this.toggleSelect(this.curr.key);
					}
					break;

				case "PageDown":
					if(this.isOpen)
					{
						e.preventDefault();
						this.keyEvent = true;
					
						this.curr = this.shown[last];
						this.focusOption(this.curr.key);

						if(!this.multiple && !this.isSelected(this.curr.key))
							this.toggleSelect(this.curr.key);
					}
					break;

				case "ArrowUp":
					if(this.isOpen)
					{
						e.preventDefault();
						this.keyEvent = true;

						if(curr > 0)
						{
							this.curr = this.shown[curr - 1];
							this.focusOption(this.curr.key);

							if(!this.multiple && !this.isSelected(this.curr.key))
								this.toggleSelect(this.curr.key);
						}
					}
					break;

				case "ArrowDown":
					if(this.isOpen)
					{
						e.preventDefault();
						this.keyEvent = true;

						if(curr < last)
						{
							this.curr = this.shown[curr + 1];
							this.focusOption(this.curr.key);

							if(!this.multiple && !this.isSelected(this.curr.key))
								this.toggleSelect(this.curr.key);
						}
					}
					break;
			}
		});
	}

	/**
	 * @returns {Option[]}
	 */
	getOptions()
	{
		/**
		 * @type {Option[]}
		 */
		let res = [];
		let i = 0;

		this.old.querySelectorAll(":scope > optgroup").forEach(op =>
		{
			op.querySelectorAll(":scope > option").forEach(ch =>
			{
				i++;

				res.push({
					key: i,
					title: ch.innerText.trim(),
					value: ch.value,
					img: ch.dataset.shdImg || '',
					desc: ch.dataset.shdDesc || '',
					group: op.label,
					selected: ch.selected,
					disabled: ch.disabled
				});
			});
		});
			
		this.old.querySelectorAll(":scope > option").forEach(op =>
		{
			i++;

			res.push({
				key: i,
				title: op.innerText.trim(),
				value: op.value,
				img: op.dataset.shdImg || '',
				desc: op.dataset.shdDesc || '',
				group: "",
				selected: op.selected,
				disabled: op.disabled
			});
		});

		return res;
	}

	/**
	 * 
	 * @param {Option} op 
	 * @returns {HTMLLIElement}
	 */
	buildSingleOption(op)
	{
		let item = document.createElement("li");
		item.classList.add(`${S_PREFIX}--option`);
		item.setAttribute("role", "option");
		item.id = `${this.el.id}--${op.key}`;

		let body = document.createElement("div");
		body.classList.add(`${S_PREFIX}--option__body`);

		let title = document.createElement("span");
		title.innerHTML = op.title;
		body.appendChild(title);

		let display = this.el.querySelector(`.${S_PREFIX}__display`),
			search = this.el.querySelector(`.${S_PREFIX}__search`);

		if(op.img)
		{
			let img = document.createElement("img");
			img.classList.add(`${S_PREFIX}--option__img`);
			img.src = op.img;
			img.alt = op.title;

			if(op.desc)
				img.style.marginTop = ".3rem";

			item.insertBefore(img, item.firstChild);
		}

		if(op.desc)
		{
			let desc = document.createElement("small");
			desc.innerHTML = op.desc;
			title.style.marginTop = "0px";
			body.appendChild(desc);
		}

		item.appendChild(body);

		if(!op.disabled)
		{
			item.onclick = () => this.toggleAndClose(op.key);
		
			item.onmouseenter = () =>
			{
				if(this.keyEvent)
				{
					this.keyEvent = false;
					return;
				}

				let root = this.el.querySelector(`.${S_PREFIX}__list`),
					targets = root.querySelectorAll(`.${S_PREFIX}--option`);

				targets.forEach(t => t.classList.remove("focus"));
				item.classList.add("focus");

				this.curr = op;

				if(this.search)
					if(!!op && !!op.key)
						search.setAttribute("aria-activedescendant", op.key.toString());

				if(!!op && !!op.key)
					display.setAttribute("aria-activedescendant", op.key.toString());
			}
		}

		if(op.selected)
		{
			item.classList.add("selected");
			item.setAttribute("aria-selected", "true");
		}

		if(op.disabled)
		{
			item.setAttribute("aria-disabled", "true");
			item.classList.add("disabled");
		}

		if(this.curr && this.curr.value === op.value)
			item.classList.add("focus");

		return item;
	}

	/**
	 * @param {Option[]} options 
	 */
	printOptions(options)
	{
		let list = this.el.querySelector(`.${S_PREFIX}__list`);
		list.innerHTML = "";

		let groups = options.map(op => op.group).filter(gr => !!gr);
		groups = [...new Set(groups)];

		// Options with group
		groups.forEach(gr =>
		{
			let grItem = document.createElement("li");
			grItem.classList.add(`${S_PREFIX}__group`);
			grItem.setAttribute("aria-label", gr);
			grItem.setAttribute("role", "none");
			grItem.innerHTML = gr;

			list.appendChild(grItem);

			options.filter(op => op.group === gr).forEach(top =>
				list.appendChild(this.buildSingleOption(top))
			);
		});

		// Options without group
		options.filter(op => !op.group).forEach(op =>
			list.appendChild(this.buildSingleOption(op))
		);
	}

	/**
	 * 
	 * @param {number} key
	 */
	toggleSelect(key)
	{
		/**
		 * @type {HTMLInputElement}
		 */
		let display = this.el.querySelector(`.${S_PREFIX}__display`),
			search = this.el.querySelector(`.${S_PREFIX}__search`),
			dest = this.search ? search : display;

		if(this.multiple)
			dest.focus();

		this.options.forEach(op => 
		{
			if(!this.multiple && op.key !== key)
				op.selected = false;
				
			if(op.key === key && !op.disabled)
				op.selected = this.multiple ? !op.selected : true;

			let corr = this.old.querySelector(`option[value='${op.value}']`);

			if(op.selected)
				corr.setAttribute("selected", "selected");
			else
				corr.removeAttribute("selected");
		});

		let selected = this.options.filter(op => op.selected);

		if(selected.length === 0)
			display.innerHTML = "<span>" + this.placeholder + "</span>";

		else
			display.innerHTML = selected.map(op => op.title).join(', ');

		let evt = new Event("change", {bubbles: true});
		this.old.dispatchEvent(evt);

		this.printOptions(this.shown);
	}

	/**
	 * @param {string} key 
	 */
	doSearch(key)
	{
		key = key.toLowerCase().trim();

		this.shown = this.options.filter(op =>
			(op.group && op.group.toLowerCase().includes(key)) ||
			op.title.toLowerCase().includes(key) ||
			op.value.toLowerCase().includes(key) ||
			op.desc.toLowerCase().includes(key)
		);

		if(this.shown.length === 0)
		{
			let list = this.el.querySelector(`.${S_PREFIX}__list`),
				item = document.createElement("li");

			item.classList.add(`${S_PREFIX}__noresult`);
			item.setAttribute("aria-live", "polite");
			item.innerHTML = window.SHD_LOCALES[window.LANG].NO_RESULT;

			list.innerHTML = "";
			list.appendChild(item);
		}

		else
			this.printOptions(this.shown);
	}

	init()
	{
		let el = document.querySelector('#' + this.id);
		if(el) el.parentElement.removeChild(el);

		this.options = this.getOptions();

		if(this.value.length > 0)
		{
			let varr = this.value.split(',');
			this.options.forEach(op =>
				op.selected = varr.includes(op.value)
			);
		}

		this.shown = this.options;
		this.curr = this.options.filter(op => !op.disabled)[0];

		this.el = document.createElement("div");
		this.el.classList.add(`${S_PREFIX}`);
		this.el.id = this.id;

		let display = document.createElement("button");
		display.classList.add(`${S_PREFIX}__display`);

		display.type = "button";
		display.setAttribute("role", "combobox");
		display.setAttribute("aria-multiselectable", this.multiple.toString());
		display.setAttribute("aria-label", this.name);
		display.setAttribute("aria-disabled", this.disabled.toString());
		display.setAttribute("aria-expanded", this.isOpen.toString());

		if(!!this.curr && !!this.curr.key)
			display.setAttribute("aria-activedescendant", `${this.el.id}--${this.curr.key}`);

		display.innerHTML = this.options.filter(op => op.selected).map(op => op.title).join(',');
		if(display.innerHTML.length === 0)
			display.innerHTML = "<span>" + this.placeholder + "</span>";

		display.onclick = () =>
		{
			if(this.isOpen)
				this.close();

			else if(!this.disabled)
				this.open();
		};

		this.el.appendChild(display);

		let menu = document.createElement("div");
		menu.classList.add(`${S_PREFIX}__menu`);
		menu.id = `${this.el.id}_menu`;
		
		if(this.search)
		{
			let search = document.createElement("input");
			search.classList.add(`${S_PREFIX}__search`);
			search.setAttribute("role", "searchbox");
            search.setAttribute("aria-label", window.SHD_LOCALES[window.LANG].SEARCH);
			search.setAttribute("aria-autocomplete", "list");
			search.setAttribute("aria-controls", `${this.el.id}_list`);

			if(this.curr && this.curr.key)
				search.setAttribute("aria-activedescendant", `${this.el.id}--${this.curr.key}`);

			search.autocapitalize = "none";
			search.autocomplete = "off";
			search.spellcheck = false;
			search.placeholder = window.SHD_LOCALES[window.LANG].SEARCH;
			search.type = "search";

			search.oninput = () => this.doSearch(search.value);
			menu.appendChild(search);
		}

		let list = document.createElement("ul");
		list.classList.add(`${S_PREFIX}__list`);
		list.id = `${this.el.id}_list`;
		list.setAttribute("role", "listbox");
		list.setAttribute("aria-label", this.name);
		list.setAttribute("aria-expanded", this.isOpen.toString());

		display.setAttribute("aria-owns", list.id);

		menu.appendChild(list);
		this.el.appendChild(menu);

		this.printOptions(this.options);

		this.old.parentElement.insertBefore(this.el, this.old);
		this.old.style.display = "none";

		let offTop = this.el.offsetTop,
			winHeight = window.innerHeight;

		if(offTop >= .5 * winHeight)
			this.el.classList.add("reverse");

		this.keyboardEvents();
		this.focusEvents();
	}
}