const SHD_DT_PREFIX = "shdDatatable";

window.SHD_DT_TRIG = "[data-shd-replace='datatable']";

/**
 * @class
 * @constructor
 * @public
 */
class DataTable
{
    /**
     * The table element
     * @type {HTMLTableElement}
    */
    el;

    /**
     * Array containing table's data
     * @type {object[]}
    */
    data;

    /**
     * Current page number
     * @type {number}
    */
    currPage;

    /**
     * Number of rows per page
     * @type {number}
    */
    perPage;

	/**
	 * Tells if table should have searching features
	 * @type {boolean}
	 */
	hasSearch;

	/**
	 * @param {HTMLTableElement} el - The table element
	 */
	constructor(el)
	{
		this.el = el;
        this.data = [];
        this.currPage = 1;
        this.perPage = 10;
        this.hasSearch = el.dataset.shdSearch || false;

        console.log(this.hasSearch)
	}

	/**
	 * Checks if elements has an ID assigned.
	 * If not, generate one by current UNIX timestamp
	 * This ensures good probability of unique ID
	 */
	checkId()
	{
		if(!this.el.hasAttribute("id") || this.el.id.length === 0)
			this.el.id = `${SHD_DT_PREFIX}-${+(new Date())}`;
	}

	/**
	 * Write a message that tells the rows' range currently shown
	 */
	getPgMessage()
	{
		let start = (this.currPage - 1) * this.perPage + 1
		let tot = this.el.querySelectorAll("tbody tr").length - 1;
		let end = start + tot;

		document.querySelector(`#${this.el.id}_pgdisplay`).innerHTML =
			window.SHD_LOCALES[window.LANG].SHOWING_TO
				.replace("{FROM}", start.toString())
				.replace("{TO}", end.toString())
				.replace("{SIZE}", this.data.length.toString());
	}

	/**
	 * Encapsulates table's data in an array of objects
	 * @returns {Object[]}
	 */
	getData()
	{
		let res = [],
			props = [];

		this.el.querySelectorAll("thead th").forEach(col => 
			props.push(col.innerText)
		);

		this.el.querySelectorAll("tbody > tr").forEach(row =>
		{
			let item = {
				"--bkg": row.style.backgroundColor,
			};

			row.querySelectorAll("td").forEach((col,i) =>
			{
				// each td has data-shd-value with REAL value to perform sort correctly
				// if not present, check plain text contained in cell
				item[props[i]] = col.dataset.shdValue || col.innerText
				item["display--" + props[i]] = col.innerHTML;
				
				//col.removeAttribute("data-shd-value");
			});
			res.push(item);
		});

		return res;
	}

	/**
	 * Arrange given data array into the table
	 * @param {Object[]} data - the given data array
	 */
	printData(data)
	{
		let cols = this.el.querySelectorAll("thead th"),
			tbody = this.el.querySelector("tbody");

		tbody.innerHTML = "";

		if(this.data.length === 0)
		{
			let emptyRow = document.createElement("tr"),
				emptyCol = document.createElement("td");

			emptyCol.colSpan = cols.length;
			emptyCol.style.textAlign = "center";
			emptyCol.innerHTML = window.SHD_LOCALES[window.LANG].NO_RESULT;

			emptyRow.appendChild(emptyCol);
			tbody.appendChild(emptyRow);
		}

		data.forEach(o => {
			let row = document.createElement("tr");
			row.style.backgroundColor = o["--bkg"];

			Object.keys(o).filter(k => !k.includes("--")).forEach(k =>
			{
				let col = document.createElement("td");
				col.dataset.shdValue = o[k];
				col.innerHTML = o["display--" + k];
				row.appendChild(col);
			});

			tbody.appendChild(row);
		});
	}

	/**
	 * Change current page
	 * @param {number} page - the new current page
	 */
	changePage(page)
	{
		let start = this.perPage * (page - 1);
		let end = start + this.perPage;
		let sliced = this.data.slice(start, end);

		this.currPage = page;
		this.printData(sliced);
		this.getPgMessage();
		this.pagination();
	}

	/**
	 * Filters entries by a given search key
	 * @param {string} key - the given search key
	 */
	search(key) 
	{
		key = key.toLowerCase().trim();
		let filtered = this.data.filter(d =>
		{
			let cond = false;
			for(let k in d)
				cond = cond || d[k].toLowerCase().includes(key)
			return cond;
		});

		this.currPage = 1;

		if(filtered.length === 0)
		{
			let props = Object.keys(this.data[0]),
				noresRow = document.createElement("tr"),
				noresCol = document.createElement("td");

			noresCol.setAttribute("aria-live", "polite");
			noresCol.innerHTML = window.SHD_LOCALES[window.LANG].NO_RESULT;
			noresCol.style.textAlign = "center";
			noresCol.colSpan = props.length;

			noresRow.appendChild(noresCol);
			this.el.querySelector("tbody").innerHTML = "";
			this.el.querySelector("tbody").appendChild(noresRow);
		}

		else
			this.printData(filtered.slice(0, this.perPage));

		this.pagination();
		this.getPgMessage();
	}

	/**
	 * Creates DOM elements for pagination
	 */
	pagination()
	{
		let list = document.createElement("div");
		list.classList.add(`${SHD_DT_PREFIX}__pagination`);
		list.setAttribute("role", "group");

		let totPages = Math.ceil(this.data.length / this.perPage);

		if(this.currPage > 1)
		{
			let toFirst = document.createElement("button");
			toFirst.classList.add("shdButton", "light");
			toFirst.setAttribute("aria-controls", this.el.id);
			toFirst.setAttribute("aria-label", window.SHD_LOCALES[window.LANG].GOTO_FIRST);
            toFirst.innerHTML = "<i class='la la-angle-double-left' />";

			toFirst.onclick = () => this.changePage(1);
			list.appendChild(toFirst);

			let toPrev = document.createElement("button");
			toPrev.classList.add("shdButton", "light");
			toPrev.setAttribute("aria-controls", this.el.id);
			toPrev.setAttribute("aria-label", window.SHD_LOCALES[window.LANG].GOTO_PREV);
			toPrev.innerHTML = "<i class='la la-angle-left' />";

			toPrev.onclick = () => this.changePage(this.currPage - 1);
			list.appendChild(toPrev);
		}

		for(let i = this.currPage - 5; i < this.currPage + 5; i++)
		{
			if(i < 1 || i > totPages)
				continue;

			let node = document.createElement("button");
			node.classList.add("shdButton", "light");
			node.setAttribute("aria-controls", this.el.id);
			node.setAttribute("aria-label", window.SHD_LOCALES[window.LANG].GOTO_PAGE.replace("{NUM}", i.toString()));
			node.innerHTML = i.toString();

			if(i === this.currPage)
			{
				node.classList.remove("light");
				node.classList.add("primary");
				node.setAttribute("aria-selected", "true");
			}
			else
				node.onclick = () => this.changePage(i);

			list.appendChild(node);
		}

		if(this.currPage < totPages)
		{
			let toNext = document.createElement("button");
			toNext.classList.add("shdButton", "light");
			toNext.setAttribute("aria-controls", this.el.id);
			toNext.setAttribute("aria-label", window.SHD_LOCALES[window.LANG].GOTO_NEXT);
			toNext.innerHTML = "<i class='la la-angle-right' />";

			toNext.onclick = () => this.changePage(this.currPage + 1);
			list.appendChild(toNext);

			let toLast = document.createElement("button");
			toLast.classList.add("shdButton", "light");
			toLast.setAttribute("aria-controls", this.el.id);
			toLast.setAttribute("aria-label", window.SHD_LOCALES[window.LANG].GOTO_LAST);
			toLast.innerHTML = "<i class='la la-angle-double-right' />";

			toLast.onclick = () => this.changePage(totPages);
			list.appendChild(toLast);
		}

		document.querySelector(`#${this.el.id}_pagination`).innerHTML = "";
		document.querySelector(`#${this.el.id}_pagination`)
			.appendChild(list);
	}

	/**
	 * Sorts and rearranges data into the table, by given property and way
	 * @param {string} prop - the property, according to which, to sort data 
	 * @param {number} way - sorting way, can be either 1 for "ascending" or -1 for "descending"
	 */
	changeOrder(prop, way)
	{
		let start = this.perPage * (this.currPage - 1),
			end = this.data.length > this.perPage ? start + this.perPage : this.data.length,
			sorted = this.data.sort((a,b) => a[prop] > b[prop] ? way : -way);

		this.printData(sorted.slice(start, end));
	}

	/**
	 * Toggles table's column header to change sorting according to it
	 * @param {NodeListOf<HTMLElement>} heads - List of table headers' DOM elements
	 * @param {HTMLTableHeaderCellElement} th - the target table header
	 */
	toggleHead(heads, th)
	{
		let prevStatus = th.getAttribute("aria-sort") || "";

		heads.forEach(v =>
		{
			v.removeAttribute("aria-sort");
			v.setAttribute("aria-label", `${v.innerText}: ${window.SHD_LOCALES[window.LANG].NOT_ACTIVE}`);
		});

		if(prevStatus.length === 0 || prevStatus[0] === 'd')
		{
			th.setAttribute("aria-sort", "ascending");
			th.setAttribute("aria-label", `${th.innerText}: ${window.SHD_LOCALES[window.LANG].ASC_ACTIVE}`);
			this.changeOrder(th.innerText, 1);
		}

		else
		{
			th.setAttribute("aria-sort", "descending");
			th.setAttribute("aria-label", `${th.innerText}: ${window.SHD_LOCALES[window.LANG].DESC_ACTIVE}`);
			this.changeOrder(th.innerText, -1);
		}
	}

	/**
	 * Builds all the component's logical and DOM structure
	 */
	init()
	{
		this.data = this.getData();

		this.checkId();

		// limit elements to page
		this.printData(this.data.slice(0, this.perPage));

		// creating container
		let cont = document.createElement("div");
		cont.classList.add(SHD_DT_PREFIX);

		// top panel
		let panel = document.createElement("div");
		panel.classList.add("shdRow");

		let leftCol = document.createElement("div");
		leftCol.classList.add("shdRow", "w-auto", "al-middle");

		let ppChoose = document.createElement("select");
		ppChoose.style.marginRight = "10px";
		ppChoose.classList.add("shd-control");

		// populating per-page select
		for(let i = 1; i <= 5; i++)
			ppChoose.innerHTML += `
				<option ${i === 1 ? 'selected' : ''} value='${i * 10}'>
					${i * 10}
				</option>
			`;

		ppChoose.onchange = () =>
		{
			this.perPage = parseInt(ppChoose.value);
			this.currPage = 1;
			this.printData(this.data.slice(0, this.perPage));
			this.pagination();
			this.getPgMessage();
		};

		let ppString = document.createElement("span");
		ppString.innerHTML = window.SHD_LOCALES[window.LANG].PER_PAGE;

		leftCol.appendChild(ppChoose);
		leftCol.appendChild(ppString);
		panel.appendChild(leftCol);

		/*
		BUG: cannot use if(this.hasSearch). Even if this.hasSearch is false,
		     that condition returns always true.
		 */
		if(this.hasSearch === true)
		{
			let rightCol =  document.createElement("div");
			rightCol.classList.add("shdCol", "w-4", "right");

			let searchInput = document.createElement("input");
			searchInput.setAttribute("aria-controls", this.el.id);
			searchInput.setAttribute("role", "searchbox");
			searchInput.classList.add("shd-control");
			searchInput.placeholder = window.SHD_LOCALES[window.LANG].SEARCH;
			searchInput.type = "search";

			searchInput.addEventListener("input", () =>
				this.search(searchInput.value)
			)

			rightCol.appendChild(searchInput);
			panel.appendChild(rightCol);
		}

		cont.appendChild(panel);

		// sorting headers
		let heads = this.el.querySelectorAll("thead th");
		heads.forEach(th =>
		{
			if(th.hasAttribute("data-shd-static"))
			{
				th.removeAttribute("data-shd-static");
				return;
			}

			th.setAttribute("tabindex", "0");
			th.setAttribute("scope", "shdCol");
			th.setAttribute("aria-controls", this.el.id);
			th.setAttribute("aria-label", `${th.innerText}: ${window.SHD_LOCALES[window.LANG].NOT_ACTIVE}`);

			th.addEventListener("click", () => this.toggleHead(heads, th));

			th.addEventListener("keyup", e =>
			{
				if(document.activeElement === th && e.key === "Enter")
					this.toggleHead(heads, th);
			});
		});

		// inserting container
		let tableCont = document.createElement("div");
		tableCont.classList.add(`${SHD_DT_PREFIX}__table`);

		this.el.parentElement.insertBefore(cont, this.el);
		cont.appendChild(tableCont);
		tableCont.appendChild(this.el);

		// pagination
		let pagPanel = document.createElement("div");
		pagPanel.classList.add("shdRow", "al-middle");

		let pgLcol = document.createElement("div");
		pgLcol.classList.add("shdCol", "w-auto");
		pgLcol.setAttribute("aria-live", "polite");
		pgLcol.id = `${this.el.id}_pgdisplay`;

		pagPanel.appendChild(pgLcol);

		let pgRCol = document.createElement("div");
		pgRCol.classList.add("shdCol", "w-auto", "right", "al-right");
		pgRCol.id = `${this.el.id}_pagination`;

		pagPanel.appendChild(pgRCol);
		cont.appendChild(pagPanel);

		this.pagination();
		this.getPgMessage();

		this.el.removeAttribute("data-shd-search");
		this.el.removeAttribute("data-shd-locale");
		this.el.removeAttribute("data-shd-replace");
	}
}