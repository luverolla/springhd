/**
 * @typedef {object} Field
 * @property {string} name
 * @property {FieldValues} values
 * 
 * @property {boolean} [required]
 * @property {number} [minval]
 * @property {number} [maxval]
 * @property {number} [eqval]
 * @property {string} [pattern]
 * @property {string} [example]
 * @property {number} [min]
 * @property {number} [max]
 * @property {number} [minlen]
 * @property {number} [maxlen]
 * @property {number} [eqlen]
 */

/**
 * @typedef {HTMLInputElement|HTMLSelectElement} FieldType
 * @typedef {string[]|FileList} FieldValues
 */

const SIZES = [
	"bytes", 
	"kilobytes", 
	"megabytes",
	"gigabytes"
];

 /**
  * Check if file satisfy size condition
  * @param {File} file 
  * @param {string} size
  * @param {"min" | "max"} crit
  * @returns {boolean}
  */
function fileSizeCond(file, size, crit)
{
	let letter = size[size.length - 1].toLowerCase(),
		mul = SIZES.findIndex(s => s[0] === letter),
		bytes = parseFloat(size) * Math.pow(2, 10 * mul);

	let cond = !!file;

	if(crit === "min")
		cond = cond && file.size >= bytes;
	else
		cond = cond && file.size <= bytes;

	return cond;
}

/**
 * Retrieves field values based on field properties
 * @param {FieldType} field 
 * @returns {FieldValues}
 */
function getValues(field)
{
	if(field.getAttribute("role") === "radiogroup")
	{
		let sel = field.querySelector(":checked");
		return sel ? [sel] : [];
	}

    if(field.tagName === "TEXTAREA")
        return [field.innerText];

	else if(field.tagName === "SELECT")
	{
		return Array.from(field.querySelectorAll("option")).map(op =>
		{
			if(op.selected)
				return op.value.toString();
		});
	}
	
	else if(field.tagName === "INPUT")
	{
		if(field.type === "file")
			return field.files;

		else if(field.type === "checkbox")
			return field.checked ? ["1"] : [];

		else if(field.type === "radio")
			return field.checked ? [field.value] : [];

		else
			return field.value.split(',');
	}

	// fallback
	return [];
}

const F_PREFIX = "shdForm";
const F_TRIG = "[data-shd-replace='form']";

window.F_TRIG = F_TRIG;
window.SHD_FORM_ISTANCES = {};

const CONDS =
{
	"required": (v) => v.some(e => !!e && e.length > 0),
	"minval": (v,a) => v.length >= a,
	"maxval": (v,a) => v.length <= a,
	"eqval": (v,a) => v.length === a,
	"pattern": (v,a) => a.match(v[0]),
	"min": (v,a) => parseFloat(v[0] || "0") >= a,
	"max": (v,a) => parseFloat(v[0] || "0") <= a,
	"minsize": (v,a) => v.length == 0 || fileSizeCond(v[0], a, "min"),
	"maxsize": (v,a) => v.length == 0 || fileSizeCond(v[0], a, "max"),
	"minlen": (v,a) => v[0].length >= a,
	"maxlen": (v,a) => v[0].length <= a,
	"eqlen": (v,a) => v[0].length === a
};

const KEYS = Object.keys(CONDS);

class SHDForm
{
	/**
	 * @private
	 * @type {HTMLFormElement}
	 */
	el;

	/**
	 * @private
	 * @type {string}
	 */
	name;

	/**
	 * @private
	 * @type {Field[]}
	 */
	fields = [];

	/**
	 * @param {string} name
	 */
	static reload(name)
	{
		let obj = window.SHD_FORM_ISTANCES[name];
		if(obj)
		{
			obj.reset(obj.el);
			obj.init();
		}
	}

	/**
	 * @param {HTMLFormElement} el
	 */
	reset(el)
	{
		this.el = el;
		this.fields = [];

		this.name = el.dataset.shdName || Date.now().toString();
		this.el.name = this.name;
		this.id = `${F_PREFIX}-${this.name}`;
	}

	/**
	 * @param {HTMLFormElement} el 
	*/
	constructor(el)
	{
		this.reset(el);
	}

	getFields()
	{
		[...this.el.elements].forEach(fd =>
		{
			// no name = no field
			if(!fd.hasAttribute("name"))
				return;

			// hidden field, no need for container
			if(fd.type === "hidden")
				return;

			// field with same name already registered
			if(this.fields.find(f => f.name === fd.name))
				return;

			let obj = {
				name: fd.name,
				values: getValues(fd)
			};

			let cnt = this.el.querySelector(`[data-shd-field='${fd.name}']`);
			KEYS.forEach(k => 
			{
				obj[k] = cnt.getAttribute(`data-shd-${k}`);
				cnt.removeAttribute(`data-shd-${k}`);
			});

			this.fields.push(obj);
		});
	}

	printFields()
	{
		this.fields.forEach(f =>
		{
			/** @type {FieldType} */
			let field = this.el.querySelector(`[name='${f.name}']`),
				label = this.el.querySelector(`[data-shd-label='${f.name}']`),
				helper = this.el.querySelector(`[data-shd-helper='${f.name}']`),
				cont = this.el.querySelector(`[data-shd-field='${f.name}']`);

			cont.id = `${this.id}--${f.name}`;
			cont.classList.add(`${F_PREFIX}--field`);

			field.id = `${this.id}--${f.name}_target`;

			if(field.tagName !== "FIELDSET")
				field.classList.add("shd-control");

			if(label)
			{
				label.classList.add(`${F_PREFIX}--field__label`);
				label.removeAttribute("data-shd-label");
				label.id = `${cont.id}_label`;
				label.htmlFor = field.id;
			}

			if(helper)
			{
				helper.classList.add(`${F_PREFIX}--field__helper`);
				helper.id = `${cont.id}_helper`;
				helper.removeAttribute("data-shd-for");
				field.setAttribute("aria-describedby", helper.id);
			}

			[...field.attributes].forEach(at =>
			{
				let arr = at.name.split('-'),
					shdName = arr[arr.length - 1];

				if(KEYS.includes(shdName))
					field.removeAttribute(at.name);
			});

			let err = document.createElement("div");
			err.classList.add(`${F_PREFIX}__error`);
			err.id = `${cont.id}_error`;
			err.dataset.shdFor = f.name;
			cont.appendChild(err);

			field.addEventListener("change", () =>
			{
				this.fields.find(e => e.name === f.name).values = getValues(field);
				this.validateField(f.name);
			});

			field.addEventListener("shd-change", () =>
			{
				this.fields.find(e => e.name === f.name).values = getValues(field);
				this.validateField(f.name);
			});
		});
	}

	/**
	 * @param {string} name
	 * @returns {boolean}
	 */
	validateField(name)
	{
		let id = `${this.id}--${name}`,
			lf = this.fields.find(f => f.name === name),
			fd = this.el.querySelector(`[name='${name}']`),
			er = this.el.querySelector(`#${id}_error`),
			cnt = this.el.querySelector(`#${id}`);

		let errors = [];
		lf.valid = true;
		er.innerHTML = "";

		KEYS.forEach(k =>
		{
			let single = !lf[k] || !!CONDS[k](lf.values, lf[k]);
			lf.valid = lf.valid && single;

			if(!single)
			{
				let msg = window.SHD_LOCALES[window.LANG].ERR_SPEC[k].replace(`{${k}}`, lf[k]);

				if(fd.type === "file")
				{
					let letter = lf[k].split('')[lf[k].length - 1],
						unit = SIZES.find(s => s[0] === letter.toLowerCase()),
						newval = lf[k].slice(0, lf[k].length - 1) + " " + unit;

					msg = window.SHD_LOCALES[window.LANG].ERR_SPEC[k].replace(`{${k}}`, newval);
				}

				errors.push(msg);
			}
		});

		if(lf.valid)
		{
			er.innerHTML = "";
			cnt.classList.remove("invalid");
		}

		else
		{
			er.innerHTML = `<strong>${window.SHD_LOCALES[window.LANG].ERR_GEN}:</strong>`;

			let list = document.createElement("ul");
			errors.forEach(e => list.innerHTML += `<li>${e}</li>`);

			er.setAttribute("aria-live", "polite");
			er.appendChild(list);

			cnt.classList.add("invalid");
		}
		
		return lf.valid;
	}

	/**
	 * @returns {boolean}
	 */
	validate()
	{
		let valid = true;

		for(let f of this.fields)
		{
			let single = this.validateField(f.name);
			valid = valid && single;
		}

		return valid;
	}

	/**
	 * @param {Event} e 
	 */
	onSubmit(e)
	{
		e.preventDefault();
		if(this.validate())
			this.el.submit();
	}

	init()
	{
		this.el.id = this.id;
		this.el.classList.add(F_PREFIX);

		this.getFields();
		this.printFields();

		this.el.onsubmit = (e) => this.onSubmit(e);
		this.el.removeAttribute("data-shd-name");
		this.el.removeAttribute("data-shd-locale");
		this.el.removeAttribute("data-shd-replace");
	}
}