const CK_PREFIX = "shdCheckbox";
const CK_TRIG = window.CK_TRIG;

class SHDCheckbox
{
	/**
	 * @type {HTMLElement}
	 */
	el;

	/**
	 * @type {string}
	 */
	name;

	/**
	 * @type {string}
	 */
	id;

	/**
	 * @type {boolean}
	 */
	disabled;

	/**
	 * @param {HTMLElement} el
	 */
	constructor(el)
	{
		this.el = el;
		this.name = el.name || Date.now().toString();
		this.id = `${CK_PREFIX}-${this.name}`;
	}

	toggleCheck()
	{
		this.el.classList.toggle("selected");
		this.el.setAttribute("aria-checked", !this.el.getAttribute("aria-checked"));
	}

	init()
	{
		let input = this.el.querySelector("input");
		input.id = this.id;

		this.el.addEventListener("focusin", () => 
		{
			if(document.activeElement === input)
				this.el.classList.add("focus")
		});

		this.el.addEventListener("focusout", () =>
			this.el.classList.remove("focus")
		);

		this.el.classList.add(CK_PREFIX);
		this.el.htmlFor = this.id;
		this.el.setAttribute("aria-checked", input.checked);
		this.el.setAttribute("aria-disabled", input.disabled);

		if(input.disabled)
			this.el.classList.add("disabled");

		if(input.checked)
			this.el.classList.add("selected");

		this.el.addEventListener("click", () => 
		{
			if(!this.disabled)
				this.toggleCheck()
		});

		this.el.removeAttribute("data-shd-replace");

		let evt = new CustomEvent("shd-field-create");
		window.dispatchEvent(evt);

		// sleep for 50 ms (to make sure that next checkbox will have different id)
		let now = new Date().getTime();
		while(new Date().getTime() < now + 50){;}
	}
}