window.SHD_ED_TRIG = "[data-shd-replace='editor']";

class SHDEditor
{
    /**
     * @type {HTMLTextAreaElement}
     */
    el;

    /**
     * @type {FroalaEditor}
     */
    editor;

    /**
     * @param {HTMLTextAreaElement} el
     */
    constructor(el)
    {
        this.el = el;
    }

    init()
    {
        this.editor = ClassicEditor
            .create(this.el,
            {
                language: window.LANG,
                toolbar:
                {
					items: [
                        'heading', '|', 'bold', 'italic', 'underline', 'strikethrough', 'removeFormat', '|', 'link', 'bulletedList', 'numberedList', '|',
						'alignment', 'outdent', 'indent', '|', 'imageUpload', 'blockQuote', 'insertTable', 'mediaEmbed',
						'|', 'undo', 'redo'
					]
				},
				image:
                {
					toolbar: ['imageTextAlternative', 'imageStyle:full', 'imageStyle:side']
				},
				table:
                {
					contentToolbar: ['tableColumn', 'tableRow', 'toggleTableCaption', 'mergeTableCells']
				}
            })
            .then(editor =>
            {
                editor.setData("<p></p><p></p><p></p>");

                editor.model.document.on('change:data', () =>
                {
                    this.el.innerHTML = editor.getData();
                    let evt = new Event("change");
                    this.el.dispatchEvent(evt);
                });
            })
            .catch(error =>
            {
                console.error(error);
            });

        this.el.removeAttribute("data-shd-editor");
        this.el.removeAttribute("data-shd-locale");
    }
}