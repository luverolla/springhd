window.SHD_MENU_TRIG = "[data-shd-menu]";

class SHDMenu
{
    /**
     * @type {HTMLElement}
     */
    trig;
    
    /**
     * @type {HTMLUlElement}
     */
    el;
    
    /**
     * @type {string}
     */
    id;
    
    /**
     * @type {boolean}
     */
    shown;
    
    constructor(trig)
    {
        this.trig = trig;
        this.id = trig.dataset.shdMenu;
        this.el = document.getElementById(this.id);
    }
    
    show()
    {
        this.shown = true;
        this.trig.classList.add("open");
        this.el.setAttribute("aria-expanded", "true");
    }
    
    hide()
    {
        this.shown = false;
        this.trig.classList.remove("open");
        this.el.setAttribute("aria-expanded", "false");
    }
    
    init()
    {
        this.trig.tabIndex = 0;
        this.trig.classList.add("shdMenu");

        this.el.classList.add("shdMenu__el");
        this.el.style.top = this.trig.innerHeight + "px";
        
        this.trig.setAttribute("aria-controls", this.id);
        this.trig.removeAttribute("data-shd-menu");

		this.trig.addEventListener("mouseover", () =>
		{
			document.querySelectorAll(".shdMenu").forEach(e =>
				e.classList.remove("open")
			);
			
			this.show();
		});

        this.trig.addEventListener("focusin", () =>
        {
            document.querySelectorAll(".shdMenu").forEach(e =>
				e.classList.remove("open")
			);
			
			this.show();
        })

        document.addEventListener("click", e =>
        {
            if(e.target !== this.trig && !this.trig.contains(e.target))
                this.hide();
        });
    }
}
