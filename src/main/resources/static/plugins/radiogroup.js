window.SHD_RD_TRIG = "[data-shd-replace='radiogroup']";

const SHD_RD_PREFIX = "shdRadioGroup";
const SHD_RD_ITEM_TRIG = "[type=radio]";
const SHD_RD_TRIG = window.SHD_RD_TRIG;

class SHDRadioGroup
{
	/**
	 * @type {HTMLElement}
	 */
	el;

	/**
	 * @type {string}
	 */
	id;

	/**
	 * @type {string}
	 */
	name;

	/**
	 * @type {boolean}
	 */
	disabled;

	
	/**
	 * @param {HTMLElement} el
	 */
	constructor(el)
	{
		this.el = el;
		this.disabled = el.hasAttribute("disabled");

		this.name = el.name || Date.now().toString();
		this.id = `${SHD_RD_PREFIX}-${this.name}`;
	}

	/**
	 * @param {string} val 
	 */
	checkOption(val)
	{
		this.el.querySelectorAll("[type=radio]").forEach(op =>
		{
			if(op.value == val)
			{
				op.setAttribute("aria-checked", true);
				op.parentElement.classList.add("selected");
			}

			else
			{
				op.setAttribute("aria-checked", false);
				op.parentElement.classList.remove("selected");
			}
		});
	}


	init()
	{
		// focus events
		this.el.addEventListener("focusin", () => 
		{
			if(document.activeElement.tagName == "INPUT")
				this.el.classList.add("focus")
		});

		this.el.addEventListener("focusout", () => 
			this.el.classList.remove("focus")
		);

		this.el.id = this.id;
		this.el.classList.add(SHD_RD_PREFIX);
		this.el.setAttribute("role", "radiogroup");

		if(this.el.dataset.shdDisplay == "inline")
			this.el.classList.add("inline");

		if(this.disabled)
			this.el.classList.add("disabled");

		this.el.querySelectorAll("[type=radio]").forEach(op =>
		{
			op.name = this.name;
			op.id = `${this.id}--${op.value}`;
			op.parentElement.htmlFor = op.id;
			op.setAttribute("role", "radio");
			op.setAttribute("aria-checked", op.checked);
			op.parentElement.classList.add(`${SHD_RD_PREFIX}--option`);

			if(op.disabled)
				op.parentElement.classList.add("disabled");

			op.addEventListener("click", () => this.checkOption(op.value));

			op.addEventListener("change", () =>
			{
				let evt = new CustomEvent("shd-change");
				this.el.dispatchEvent(evt);
			});
		});

		this.el.removeAttribute("data-shd-display");
		this.el.removeAttribute("data-shd-replace");
		this.el.removeAttribute("data-shd-onchange");

		let evt = new CustomEvent("shd-field-create");
		window.dispatchEvent(evt);
	}
}