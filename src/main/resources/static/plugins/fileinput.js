window.SHD_FI_TRIG = "[data-shd-replace='fileInput']";

const SHD_FI_PREFIX = "shdFileInput";
const SHD_FI_TRIG = window.SHD_FI_TRIG;

class SHDFileInput
{
	/**
	 * @type {string}
	 */
	name;

	/**
	 * @type {string}
	 */
	id;

	/**
	 * @type {HTMLInputElement}
	 */
	old;

	/**
	 * @type {HTMLDivElement}
	 */
	el;

	/**
	 * @type {string} 
	 */
	locale;
    
    /**
     * 
     * @param {string} text
     */
    text;

	/**
	 * @param {HTMLInputElement} old 
	 */
	constructor(old)
	{
		this.old = old;
		this.name = old.name || Date.now().toString();
		this.id = `${SHD_FI_PREFIX}-${this.name}`;
        this.text = old.dataset.shdText || window.SHD_LOCALES[window.LANG].CHOOSE_FILES;
	}

	changeValue()
	{
		let files = this.old.files,
            list = this.el.querySelector("ul"),
			fd = this.el.querySelector(`.${SHD_FI_PREFIX}__value`);

		fd.innerHTML = window.SHD_LOCALES[window.LANG].N_FILES.replace("{1}", files.length);
        list.innerHTML = "";
        
        for(let i = 0; i < files.length; i++)
        {
            let it = document.createElement("li"),
                full = files[i].name,
                chunks = full.split('.'),
                name = "",
                ext = chunks[chunks.length - 1];
        
            for(let j = 0; j < chunks.length - 1; j++)
                name += "." + chunks[j];
        
            it.title = full;
            it.innerHTML = `(${ext.toUpperCase()}) - ${name.slice(1, 50)}`;
            
            if(name.slice(1).length > 50)
                it.innerHTML += "...";
            
            list.appendChild(it);
        }
	}

	init()
	{
		let parent = this.old.parentElement,
			next = this.old.nextElementSibling;

		this.old.addEventListener("change", () => 
			this.changeValue()
		);

		// create wrapper
        let wrap = document.createElement("div");
        wrap.classList.add(`${SHD_FI_PREFIX}__control`);
        
        // create container
		this.el = document.createElement("div");
		this.el.classList.add(SHD_FI_PREFIX);

		this.el.addEventListener("focusin", () => 
		{
			if(document.activeElement.tagName === "INPUT")
				this.el.classList.add("focus");
		});

		this.el.addEventListener("focusout", () =>
			this.el.classList.remove("focus")
		);
		
		// create "choose" button
		let btn = document.createElement("span");
        btn.role = "button";
		btn.classList.add(`${SHD_FI_PREFIX}__btn`);
		btn.innerHTML = this.text;
		btn.appendChild(this.old);
        
        // create "clear" button
        let cbt = document.createElement("span");
        cbt.role = "button";
        cbt.classList.add(`${SHD_FI_PREFIX}__cbt`);
        cbt.innerHTML = "<i class='la la-trash text-error'></i>";
        cbt.title = window.SHD_LOCALES[window.LANG].CLEAR_FILES;
        
        cbt.onclick = () =>
        {
            this.el.querySelector("ul").innerHTML = "";
            this.old.value = null;
        };

		// create value field
		let value = document.createElement("span");
		value.classList.add(`${SHD_FI_PREFIX}__value`);
        
        // create file list
        let list = document.createElement("ul");
        list.setAttribute("aria-live", "assertive");
        list.classList.add(`${SHD_FI_PREFIX}__list`);

		wrap.appendChild(btn);
        wrap.appendChild(cbt);
		wrap.appendChild(value);
        
        this.el.appendChild(wrap);
        this.el.appendChild(list);

		parent.insertBefore(this.el, next);

		window.dispatchEvent(new CustomEvent("shd-field-create"));
	}
}