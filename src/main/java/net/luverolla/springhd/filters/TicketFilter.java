package net.luverolla.springhd.filters;

import lombok.Data;

@Data
public class TicketFilter
{
    private String code;
    private String status;
    private String priority;
    private String object;
    private String author;
    private String operator;
    private String dateFrom;
    private String dateTo;
    private String lupFrom;
    private String lupTo;
    private String category;
}
