package net.luverolla.springhd.filters;

import lombok.Data;

@Data
public class LogFilter
{
    private String dateFrom;
    private String timeFrom;
    private String dateTo;
    private String timeTo;
    private String actor;
    private String text;
    private String type;
    private String action;
    private String target;
}
