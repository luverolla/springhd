package net.luverolla.springhd.filters;

import lombok.Data;

@Data
public class CategoryFilter
{
    private String name;
}
