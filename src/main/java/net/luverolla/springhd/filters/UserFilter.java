package net.luverolla.springhd.filters;

import lombok.Data;

@Data
public class UserFilter
{
    private String code;
    private String name;
    private String role;
    private String surname;
    private String username;
    private String dateFrom;
    private String dateTo;
}
