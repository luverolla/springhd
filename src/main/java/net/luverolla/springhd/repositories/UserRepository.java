package net.luverolla.springhd.repositories;

import net.luverolla.springhd.entities.User;

import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long>
{

}
