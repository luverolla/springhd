package net.luverolla.springhd.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import net.luverolla.springhd.entities.Answer;

public interface AnswerRepository extends JpaRepository<Answer, Long> {}
