package net.luverolla.springhd.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import net.luverolla.springhd.entities.Ticket;

public interface TicketRepository extends JpaRepository<Ticket, Long> {}
