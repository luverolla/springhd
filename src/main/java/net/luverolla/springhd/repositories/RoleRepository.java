package net.luverolla.springhd.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import net.luverolla.springhd.entities.Role;

public interface RoleRepository extends JpaRepository<Role, Long> {

}
