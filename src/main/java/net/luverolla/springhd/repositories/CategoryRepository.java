package net.luverolla.springhd.repositories;

import net.luverolla.springhd.entities.Category;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryRepository extends JpaRepository<Category, Long> {}
