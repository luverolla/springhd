package net.luverolla.springhd.controllers;

import net.luverolla.springhd.entities.Category;
import net.luverolla.springhd.filters.CategoryFilter;
import net.luverolla.springhd.services.CategoryService;
import net.luverolla.springhd.services.TicketService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class CategoryController
{
    @Autowired
    private CategoryService service;
    
    @Autowired
    private TicketService ts;
    
    @PostMapping("/admin/addCategory")
    public String addCategoryDo(@ModelAttribute("category") Category category)
    {
        service.save(category);
        
        return "redirect:listCategories";
    }
    
    @RequestMapping("/admin/listCategories")
    public String listCategoriesView(Model m, @ModelAttribute("filter") CategoryFilter f)
    {
        m.addAttribute("viewClass", "tickets");
        m.addAttribute("viewName", "categoriesList");
        m.addAttribute("category", new Category());
        m.addAttribute("categoryList", service.filter(f));
        m.addAttribute("ticketService", ts);
        
        return "admin/listCategories";
    }
}
