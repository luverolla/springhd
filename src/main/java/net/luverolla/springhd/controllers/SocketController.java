package net.luverolla.springhd.controllers;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class SocketController
{
	@Autowired
	private SimpMessagingTemplate smt;
	
	@Scheduled(fixedDelay = 500, initialDelay = 500)
    private void send()
	{
		smt.convertAndSend("/topic/datetime", new Date().toString());
	}
}
