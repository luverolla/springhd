package net.luverolla.springhd.controllers;

import net.luverolla.springhd.entities.User;
import net.luverolla.springhd.filters.UserFilter;
import net.luverolla.springhd.services.CodeService;
import net.luverolla.springhd.services.LocaleService;
import net.luverolla.springhd.services.RoleService;
import net.luverolla.springhd.services.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@Controller
public class UserController
{
	@Autowired
	private UserService service;

	@Autowired
	private CodeService cs;

	@Autowired
	private RoleService rs;

    @Autowired
    private LocaleService locs;

	@RequestMapping("/admin/listUsers")
	public String listUserView(Model m, @ModelAttribute("filter") UserFilter fil, HttpServletRequest req)
	{
		m.addAttribute("filter", fil);
		m.addAttribute("userList", service.filter(service.allUsers(), fil, locs.locale(req)));
		m.addAttribute("viewClass", "users");
		m.addAttribute("viewAct", "list");
        m.addAttribute("pattern", locs.datePattern(req));

		return "/admin/listUsers";
	}

	@RequestMapping("/admin/listOperators")
	public String listOperatorsView(Model m, @ModelAttribute("filter") UserFilter fil, HttpServletRequest req)
	{
		m.addAttribute("filter", fil);
		m.addAttribute("userList", service.filter(service.allOperators(), fil, locs.locale(req)));
		m.addAttribute("viewClass", "operators");
		m.addAttribute("viewAct", "list");
        m.addAttribute("pattern", locs.datePattern(req));

		return "/admin/listOperators";
	}

	@GetMapping("/admin/addUser")
	public String addUserView(Model m)
	{
		m.addAttribute("viewClass", "users");
		m.addAttribute("viewAct", "add");
		m.addAttribute("user", new User());

		return "admin/addUser";
	}

	@GetMapping("/admin/addOperator")
	public String addOperatorView(Model m)
	{
		m.addAttribute("viewClass", "operators");
		m.addAttribute("viewAct", "add");
		m.addAttribute("operator", new User());

		return "admin/addOperator";
	}

	@PostMapping("/admin/addUser")
	public String addUserDo(@ModelAttribute("user") User u)
	{
		u.setCode(cs.nextCode());
		u.setUsername(service.nextUsername(u));
		u.setRole(rs.findOne("USER"));
		u.setPassword(new BCryptPasswordEncoder().encode(u.getPassword()));
		
		service.save(u);
		return "redirect:/admin/listUsers";
	}

	@PostMapping("/admin/addOperator")
	public String addOperatorDo(@ModelAttribute("operator") User u)
	{
		u.setCode(cs.nextCode());
		u.setUsername(service.nextUsername(u));
		u.setRole(rs.findOne("OPERATOR"));
		u.setPassword(new BCryptPasswordEncoder().encode(u.getPassword()));
		
		service.save(u);
		return "redirect:/admin/listOperators";
	}
}
