package net.luverolla.springhd.controllers;

import java.net.URLConnection;
import java.security.Principal;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.luverolla.springhd.entities.Ticket;
import net.luverolla.springhd.filters.TicketFilter;
import net.luverolla.springhd.services.CodeService;
import net.luverolla.springhd.services.TicketService;
import net.luverolla.springhd.services.UserService;
import net.luverolla.springhd.entities.File;
import net.luverolla.springhd.entities.User;
import net.luverolla.springhd.security.UserDetailsImpl;
import net.luverolla.springhd.services.CategoryService;
import net.luverolla.springhd.services.FileService;
import net.luverolla.springhd.services.LocaleService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.core.io.Resource;
import org.springframework.web.bind.annotation.*;

import org.apache.tomcat.util.http.fileupload.IOUtils;

@Controller
public class TicketController
{
    @Autowired
    private TicketService service;

    @Autowired
    private UserService us;

    @Autowired
    private CodeService cos;
    
    @Autowired
    private CategoryService cs;
    
    @Autowired
    private FileService fs;
    
    @Autowired
    private LocaleResolver lr;
    
    @Autowired
    private LocaleService locs;

    @GetMapping("/admin/addTicket")
    public String addTicketView(Model m)
    {
        m.addAttribute("ticket", new Ticket());
        m.addAttribute("userList", us.allOperators());
        m.addAttribute("categories", cs.findAll());
        m.addAttribute("viewClass", "tickets");
        m.addAttribute("viewAct", "add");

        return "admin/addTicket";
    }

    @PostMapping("/admin/addTicket")
    public String addTicketDo(Model m,
                              @ModelAttribute("ticket") Ticket tk,
                              @RequestParam("files") MultipartFile[] files,
                              @RequestParam("op") Optional<String> op,
                              @RequestParam("cat") String cat)
    throws Exception
    {
        tk.setCode(cos.nextCode());
        tk.setAuthor(us.findByUsername("_admin"));
        tk.setCategory(cs.findByCode(cat));
        tk.setPriority(Ticket.Priority.NORMAL);
        tk.setStatus(Ticket.Status.OPEN);
        service.save(tk);
        
        op.ifPresent(s -> tk.setOperator(us.findByCode(s)));
        
        for(var file : files)
        {
            File f = new File();
            f.setName(file.getOriginalFilename());
            f.setCode(cos.randCode(20));
            f.setTicket(tk);
            fs.save(file, f, tk.getCode());
        }

        service.save(tk);
        return "redirect:/admin/listTickets";
    }

    @GetMapping("/admin/listTickets")
    public String listTicketsView(Model m, @ModelAttribute("filter") TicketFilter fil, HttpServletRequest req)
    {
        
        m.addAttribute("filter", fil);
        m.addAttribute("ticketList", service.filter(fil, lr.resolveLocale(req)));
        m.addAttribute("userList", us.allOperators());
        m.addAttribute("datePattern", locs.datePattern(lr.resolveLocale(req)));
        m.addAttribute("categoryList", cs.findAll());
        m.addAttribute("viewClass", "tickets");
        m.addAttribute("viewAct", "list");

        return "admin/listTickets";
    }
    
    @GetMapping("/resource/{code}/{hash}")
    public void getFile(@PathVariable("code") String code,
                        @PathVariable("hash") String hash,
                        Principal principal,
                        HttpServletResponse res) throws Exception
    {
        Ticket tg = service.findByCode(code);
        User au = ((UserDetailsImpl)principal).getUser();
        
        // authenticated as user and not own ticket
        if(au.getRole().getName().contains("USER") && !tg.getAuthor().equals(au))
            res.sendError(403);
        
        // authenticated as operator but not own assigned ticket
        if(au.getRole().getName().contains("OPER") && tg.getOperator() != null && !tg.getOperator().equals(au))
            res.sendError(403);
        
        Resource rs = fs.load(code + '/' + hash);
        res.setContentType(URLConnection.guessContentTypeFromName(fs.resolvePath(code + '/' + hash)));
        IOUtils.copy(rs.getInputStream(), res.getOutputStream());
        res.flushBuffer();
    }
}
