package net.luverolla.springhd.security;

import net.luverolla.springhd.entities.User;
import net.luverolla.springhd.services.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public class UserDetailsServiceImpl implements UserDetailsService
{
	@Autowired
	private UserService service;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException
	{
		User user = service.findByUsername(username);

		if (user == null)
			throw new UsernameNotFoundException("Could not find user");

		return new UserDetailsImpl(user);
	}
}

