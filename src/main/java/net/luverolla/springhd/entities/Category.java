package net.luverolla.springhd.entities;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

import javax.persistence.*;
import java.util.Set;
import org.hibernate.annotations.CreationTimestamp;

@Data
@Entity
@Table(name = "categories")
public class Category implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    
    @Column(unique = true, updatable = false)
    private String code;
    
    @Column(updatable = false)
    @CreationTimestamp
    private Date createdAt;

    @Column(nullable = false)
    private String name;

    @OneToMany(mappedBy = "category")
    private Set<Ticket> tickets;
}
