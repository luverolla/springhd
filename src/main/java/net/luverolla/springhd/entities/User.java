package net.luverolla.springhd.entities;

import lombok.Data;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Data
@Entity
@Table(name = "users")
public class User
{
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long id;
	
	@Column(updatable = false)
	@CreationTimestamp
	private Date createdAt;
	
	@Column
	@UpdateTimestamp
	private Date lastUpdate;
	
	@Column(unique = true)
	private String code;
	
	@Column(unique = true)
	private String username;
	
	@Column
	private String password;
	
	@Column
	private String name;
	
	@Column 
	private String surname;
	
	@ManyToOne
    @JoinColumn(name = "role_id")
    private Role role;

	@ManyToOne
	@JoinColumn(name = "operator_id")
	private User operator;

	@OneToMany(mappedBy = "operator")
	private Set<User> assignedUsers;
	
	@OneToMany(mappedBy = "author")
	private Set<Ticket> tickets;

	@OneToMany(mappedBy = "operator")
	private Set<Ticket> assignedTickets;
	
	@OneToMany(mappedBy = "author")
	private Set<Answer> answers;
    
    public boolean equals(User other)
    {
        return this.getCode().equals(other.getCode());
    }
    
    public int hashCode()
    {
    	return super.hashCode();
    }

	public String getFullName()
	{
        if(this.getUsername().equals("_admin"))
            return "ADMIN";
        
        if(this.getUsername().equals("_system"))
            return "SYSTEM";
        
		return this.getName() + " " + this.getSurname();
	}
}
