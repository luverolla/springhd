package net.luverolla.springhd.entities;

import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import lombok.Data;

@Data
@Entity
@Table(name = "tickets")
public class Ticket
{
	public enum Priority {LOW, NORMAL, HIGH, URGENT}
	public enum Status {OPEN, WORKING, SUSPENDED, CLOSED}
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long id;
	
	@Column(updatable = false)
	@CreationTimestamp
	private Date createdAt;
	
	@Column
	@UpdateTimestamp
	private Date lastUpdate;
	
	@Column(unique = true, nullable = false)
	private String code;
	
	@Column
	@Enumerated(EnumType.ORDINAL)
	private Priority priority;
	
	@Column
	@Enumerated(EnumType.ORDINAL)
	private Status status;
	
	@Column
	private String object;
	
	@Column(columnDefinition = "text")
	private String text;
	
	@ManyToOne
    @JoinColumn(name = "author_id", nullable = false)
    private User author;

	@ManyToOne
	@JoinColumn(name = "operator_id")
	private User operator;

	@ManyToOne
	@JoinColumn(name = "category_id")
	private Category category;
	
	@OneToMany(mappedBy = "ticket")
    private Set<Answer> answers;

	@OneToMany(mappedBy = "ticket")
	private Set<Attachment> attachments;
}
