package net.luverolla.springhd.entities;

import javax.persistence.*;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import java.util.Date;
import java.util.Set;

import lombok.Data;

@Data
@Entity
@Table(name = "answers")
public class Answer
{
	public enum Type {GENERAL, OPEN, CLOSE, SUSPEND};

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long id;
	
	@Column(updatable = false)
	@CreationTimestamp
	private Date createdAt;
	
	@Column
	@UpdateTimestamp
	private Date lastUpdate;

	@Column
	@Enumerated(EnumType.ORDINAL)
	private Type type;
	
	@Column
	private String title;
	
	@Column(columnDefinition = "text")
	private String text;
	
	@ManyToOne
    @JoinColumn(name = "author_id", nullable = false)
    private User author;
	
	@ManyToOne
    @JoinColumn(name = "ticket_id", nullable = false)
    private Ticket ticket;

	@OneToMany(mappedBy = "answer")
	private Set<Attachment> attachments;
}
