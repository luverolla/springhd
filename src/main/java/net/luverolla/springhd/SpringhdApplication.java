package net.luverolla.springhd;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import net.luverolla.springhd.entities.Category;
import net.luverolla.springhd.entities.Role;
import net.luverolla.springhd.entities.User;
import net.luverolla.springhd.repositories.RoleRepository;
import net.luverolla.springhd.repositories.UserRepository;
import net.luverolla.springhd.services.CategoryService;
import net.luverolla.springhd.services.RoleService;

@SpringBootApplication
public class SpringhdApplication
{
	public static void main(String[] args)
	{
		SpringApplication.run(SpringhdApplication.class, args);
	}
	
	@Bean
    public CommandLineRunner checkRoles(RoleRepository repo)
    {
        return (args) ->
        {
            if(repo.findAll().isEmpty())
            {
                repo.save(new Role(null, "ROLE_USER", null));
                repo.save(new Role(null, "ROLE_ADMIN", null));
                repo.save(new Role(null, "ROLE_OPERATOR", null));
            }
        };
    }
	
    @Bean
    public CommandLineRunner checkAdmins(UserRepository repo, RoleService s)
    {
        return (args) ->
        {
            if(repo.findAll().isEmpty())
            {
                User admin = new User();
                User sys = new User();

                admin.setCode("0000000001");
                admin.setUsername("_admin");
                admin.setName("Admin");
                admin.setSurname("");
                admin.setRole(s.findOne("ADMIN"));
                admin.setPassword(new BCryptPasswordEncoder().encode("root"));

                sys.setCode("0000000000");
                sys.setUsername("_system");
                sys.setName("System");
                sys.setSurname("");
                sys.setRole(s.findOne("ADMIN"));
                sys.setPassword(new BCryptPasswordEncoder().encode("s7fh453vshc53729"));

                repo.save(admin);
                repo.save(sys);
            }
    	};
    }
    
    @Bean
    public CommandLineRunner checkCategory(CategoryService cs)
    {
        return (args) ->
        {
            if(cs.findAll().isEmpty())
            {
                Category u = new Category();
                u.setCode("0000000000");
                u.setName("UNCATEGORIZED");
                cs.save(u);
            }
        };
    }
}
