package net.luverolla.springhd.services;

import net.luverolla.springhd.entities.User;
import net.luverolla.springhd.filters.UserFilter;
import net.luverolla.springhd.repositories.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@Service
public class UserService
{
    @Autowired
    private UserRepository repo;
    
    public List<User> findAll()
    {
    	return repo.findAll();
    }

    public List<User> findValid()
    {
        return this.findAll().stream()
                .filter(u -> !u.getCode().startsWith("0"))
                .collect(Collectors.toList());
    }

    public List<User> filterByUsername(String part)
    {
        return this.findAll().stream()
                .filter(u -> u.getUsername().contains(part))
                .collect(Collectors.toList());
    }
    
    public User findByUsername(String username)
    {
    	return this.findAll().stream()
    		.filter(u -> u.getUsername().equals(username))
    		.findFirst().orElse(null);
    }

    public User findByCode(String code)
    {
        return this.findAll().stream()
                .filter(u -> u.getCode().equals(code))
                .findFirst().orElse(null);
    }

    public List<User> findByRole(String roleName)
    {
        return this.findAll().stream().filter(u ->
        	u.getRole().getName().contains(roleName)
    	).collect(Collectors.toList());
    }

    public List<User> filter(List<User> base, UserFilter fil, Locale loc)
    {
        final DateFormat fmt = DateFormat.getDateInstance(DateFormat.SHORT, loc);
        
        return base.stream().filter(u ->
        {
            boolean match = true;

            if(fil.getCode() != null && fil.getCode().length() > 0)
                match = u.getCode().contains(fil.getCode());

            if(fil.getRole() != null && fil.getRole().length() > 0)
                match = match && u.getRole().getName().contains(fil.getRole());

            if(fil.getName() != null && fil.getName().length() > 0)
                match = match && u.getName().toLowerCase().contains(fil.getName().toLowerCase());

            if(fil.getSurname() != null && fil.getSurname().length() > 0)
                match = match && u.getSurname().toLowerCase().contains(fil.getSurname().toLowerCase());

            if(fil.getUsername() != null && fil.getUsername().length() > 0)
                match = match && u.getUsername().toLowerCase().contains(fil.getUsername().toLowerCase());

            if(fil.getDateFrom() != null && fil.getDateFrom().length() > 0)
            {
                try {
                    Date from = fmt.parse(fil.getDateFrom());
                    match = match && u.getCreatedAt().compareTo(from) >= 0;
                }
                catch (ParseException e) {}
            }

            if(fil.getDateTo() != null && fil.getDateTo().length() > 0)
            {
                try {
                    Date to = fmt.parse(fil.getDateTo());
                    match = match && u.getCreatedAt().compareTo(to) <= 0;
                }
                catch (ParseException e) {}
            }

            return match;

        }).collect(Collectors.toList());
    }

    public List<User> allOperators()
    {
        return this.findByRole("OPERATOR");
    }

    public List<User> allUsers()
    {
        return this.findByRole("USER");
    }

    public String nextUsername(User u)
    {
        String[] nameArr = u.getName().split("");
        String uname = (nameArr[0] + nameArr[1] + u.getSurname()).toLowerCase();
        uname = uname.replace(" ", "");

        int num = filterByUsername(uname).size();
        return num > 0 ? uname + num : uname;
    }

    public void delete(User u)
    {
       repo.delete(u);
    }

    public User save(User u)
    {
        return repo.save(u);
    }
}
