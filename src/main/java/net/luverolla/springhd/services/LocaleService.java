package net.luverolla.springhd.services;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.LocaleResolver;

@Service
public class LocaleService
{
    @Autowired
    private LocaleResolver lr;

    public Locale locale(HttpServletRequest req)
    {
        return lr.resolveLocale(req);
    }

	public String datePattern(Locale loc)
	{
		DateFormat fmt = DateFormat.getDateInstance(DateFormat.SHORT, loc);
		return ((SimpleDateFormat)fmt).toLocalizedPattern().replace("yy", "yyyy");
    }

    public String datePattern(HttpServletRequest req)
    {
        return this.datePattern(lr.resolveLocale(req));
    }
}
