package net.luverolla.springhd.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.luverolla.springhd.entities.Role;
import net.luverolla.springhd.repositories.RoleRepository;

import java.util.List;

@Service
public class RoleService
{
	@Autowired
	private RoleRepository repo;
	
	public List<Role> findAll()
	{
		return repo.findAll();
	}
	
	public Role findOne(String key) 
	{
		return this.findAll().stream()
			.filter(r -> r.getName().contains(key))
				.findFirst().orElseThrow();
	}
	
	public Role save(Role r)
	{
		return repo.save(r);
	}
}
