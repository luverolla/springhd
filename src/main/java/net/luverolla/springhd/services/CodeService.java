package net.luverolla.springhd.services;

import org.springframework.stereotype.Service;

import java.util.Random;

@Service
public class CodeService
{
    private static final String CHARS = "0123456789abcdefghijklmnopqrstuvwxyz";
    private static final int BASE = CHARS.length();
    private static final int LENGTH = 10;

    public String randCode(int len)
    {
        StringBuilder code = new StringBuilder();

        for(int i = 0; i < len; i++)
            code.append(CHARS.charAt(Math.abs(new Random().nextInt() % BASE)));

        return code.toString();
    }

    public long max(int len)
    {
        return (long) Math.pow(BASE, len);
    }

    public String nextCode()
    {
        return this.randCode(LENGTH);
    }
}
