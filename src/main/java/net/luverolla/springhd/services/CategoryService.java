package net.luverolla.springhd.services;

import net.luverolla.springhd.entities.Category;
import net.luverolla.springhd.repositories.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import net.luverolla.springhd.filters.CategoryFilter;

/**
 * Category service
 * Spring Service Bean that handle business logic related to Category entity
 * @see Category
 * @author Luigi Verolla <luverolla@outlook.com>
 */
@Service
public class CategoryService
{
    @Autowired
    private CategoryRepository repo;

    public List<Category> findAll()
    {
        return repo.findAll();
    }
    
    public List<Category> filter(CategoryFilter f)
    {
        return this.findAll().stream().filter(c ->
        {
            boolean cond = true;
            
            if(f.getName() != null && f.getName().length() > 0)
                cond = cond && c.getName().equals(f.getName());
            
            return cond;
        })
        .collect(Collectors.toList());
    }
    
    public Category save(Category c)
    {
        return repo.save(c);
    }
    
    public Category findByCode(String code)
    {
        return this.findAll().stream()
            .filter(c -> c.getCode().equals(code))
            .findFirst().orElse(null);
    }

    public Category findById(Long id)
    {
        return this.findAll().stream()
            .filter(c -> c.getId().equals(id))
            .findFirst().orElse(null);
    }
    
    public Category uncategorized()
    {
        return this.findByCode("0000000000");
    }
}
