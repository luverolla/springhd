package net.luverolla.springhd.services;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.luverolla.springhd.entities.Ticket;
import net.luverolla.springhd.entities.Category;
import net.luverolla.springhd.filters.TicketFilter;
import net.luverolla.springhd.entities.User;
import net.luverolla.springhd.repositories.TicketRepository;

@Service
public class TicketService 
{
	@Autowired
	private TicketRepository repo;
    
    public List<Ticket> findAll()
    {
        return StreamSupport
                .stream(repo.findAll().spliterator(), false)
                .collect(Collectors.toList());
    }

	public Ticket save(Ticket t)
	{
		return repo.save(t);
	}
	
	public List<Ticket> findByAuthor(User u)
	{
		return this.findAll().stream().filter(t ->
			t.getAuthor().getCode().equals(u.getCode())
		).collect(Collectors.toList());
	}
    
    public Ticket findByCode(String code)
    {
        return this.findAll().stream()
                .filter(t -> t.getCode().equals(code))
                .findFirst().orElse(null);
    }
    
    public List<Ticket> listByCategory(Category c)
    {
        return this.findAll().stream().filter(t ->
            t.getCategory() != null && t.getCategory().getId().equals(c.getId())
        ).collect(Collectors.toList());
    }

	public List<Ticket> filter(TicketFilter fil, Locale loc)
	{
		final DateFormat fmt = DateFormat.getDateInstance(DateFormat.SHORT, loc);
		
		return this.findAll().stream().filter(u ->
		{
			boolean cond = true;

			if(fil.getObject() != null && fil.getObject().length() > 0)
				cond = cond && u.getText().toLowerCase().trim().contains(fil.getObject().toLowerCase().trim());

			if(fil.getStatus() != null && fil.getStatus().length() > 0)
			{
				List<String> values = Arrays.stream(fil.getStatus().split(",")).collect(Collectors.toList());
				cond = cond && values.contains(u.getStatus().toString());
			}

			if(fil.getPriority() != null && fil.getPriority().length() > 0)
			{
				List<String> values = Arrays.stream(fil.getPriority().split(",")).collect(Collectors.toList());
				cond = cond && values.contains(u.getPriority().toString());
			}

			if(fil.getAuthor() != null && fil.getAuthor().length() > 0)
			{
				List<String> values = Arrays.stream(fil.getAuthor().split(",")).collect(Collectors.toList());
				cond = cond && values.contains(u.getAuthor().getCode());
			}

			if(fil.getOperator() != null && fil.getOperator().length() > 0)
			{
				List<String> values = Arrays.stream(fil.getOperator().split(",")).collect(Collectors.toList());
                
                if(fil.getOperator().equals("NOT_ASSIGNED"))
                    cond = cond && u.getOperator() == null;
                
                else
                    cond = cond && values.contains(u.getOperator().getCode());
			}
            
            if(fil.getCategory() != null && fil.getCategory().length() > 0)
            {
                List<String> values = Arrays.stream(fil.getCategory().split(",")).collect(Collectors.toList());
                cond = cond && values.contains(u.getCategory().getCode());
            }

			if(fil.getDateFrom() != null && fil.getDateFrom().length() > 0)
			{
				try
				{
					Date from = fmt.parse(fil.getDateFrom());
					cond = cond && u.getCreatedAt().compareTo(from) >= 0;
				}
				catch (ParseException e) {}
			}

			if(fil.getDateTo() != null && fil.getDateTo().length() > 0)
			{
				try
				{
					Date to = fmt.parse(fil.getDateTo());
					cond = cond && u.getCreatedAt().compareTo(to) <= 0;
				}
				catch (ParseException e) {}
			}
            
            if(fil.getLupFrom() != null && fil.getLupFrom().length() > 0)
			{
				try 
				{
					Date from = fmt.parse(fil.getLupFrom());
					cond = cond && u.getLastUpdate().compareTo(from) >= 0;
				}
				catch (ParseException e) {}
			}

			if(fil.getLupTo() != null && fil.getLupTo().length() > 0)
			{
				try
				{
					Date to = fmt.parse(fil.getLupTo());
					cond = cond && u.getLastUpdate().compareTo(to) <= 0;
				}
				catch (ParseException e) {}
			}
            
			return cond;

		}).collect(Collectors.toList());
	}
}
