package net.luverolla.springhd.services;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.luverolla.springhd.entities.Answer;
import net.luverolla.springhd.entities.Ticket;
import net.luverolla.springhd.repositories.AnswerRepository;

@Service
public class AnswerService
{
	@Autowired
	private AnswerRepository repo;
	
	public List<Answer> findAll()
	{
		return repo.findAll();
	}
	
	public List<Answer> findByTicket(Ticket t)
	{
		return this.findAll().stream().filter(a ->
			a.getTicket().getCode().equals(t.getCode())
		).collect(Collectors.toList());
	}
	
	public long count(Ticket t)
	{
		return this.findByTicket(t).size();
	}
}
