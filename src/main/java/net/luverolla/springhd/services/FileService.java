package net.luverolla.springhd.services;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.beans.factory.annotation.Autowired;

import net.luverolla.springhd.entities.File;
import net.luverolla.springhd.repositories.FileRepository;

@Service
public class FileService
{
    private final Path root = Paths.get("uploads");
    
    @Autowired
    private FileRepository repo;

    public void init() throws IOException
    {
        Files.createDirectory(root);
    }

    public void save(MultipartFile file, File fd, String dir) throws Exception
    {
        if(dir.contains("."))
            throw new Exception("Invalid path");

        String fname = file.getOriginalFilename();
        String path = dir + '/' + fname;

        new java.io.File(this.root.toAbsolutePath().toString() + '/' + dir).mkdirs();
        Files.copy(file.getInputStream(), this.root.resolve(path));
        
        repo.save(fd);
    }
    
    public String resolvePath(String path)
    {
        String code = path.split("/")[0],
               hash = path.split("/")[1];
        
        File found = repo.findAll().stream()
                        .filter(r -> r.getCode().equals(hash))
                        .findFirst().orElse(null);
        
        return found == null ? null : code + '/' + found.getName();
    }

    public Resource load(String filepath) throws Exception
    {
        if(filepath.contains(".."))
            throw new Exception("Invalid path");
        
        Path file = root.resolve(this.resolvePath(filepath));
        Resource resource = new UrlResource(file.toUri());

        if (resource.exists() || resource.isReadable())
            return resource;
        else
            throw new RuntimeException("Could not read the file!");     
    }

    public void deleteAll()
    {
        FileSystemUtils.deleteRecursively(root.toFile());
    }
}
